package com.hero.sidebar;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hero.sidebar.adapter.AppListActivity;
import com.htc.fragment.app.HtcListFragment;
import com.htc.widget.HtcListView;

public class MenuFragment extends HtcListFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		CustomAdapter adapter = new CustomAdapter(getActivity());

		adapter.add(new CustomItem("Home", R.drawable.ic_home));
		adapter.add(new CustomItem("Settings", R.drawable.ic_settings));
		adapter.add(new CustomItem("App Portal", R.drawable.ic_portal));

		setListAdapter(adapter);
	}

	private class CustomItem {
		public String tag;
		public int iconRes;

		public CustomItem(String tag, int iconRes) {
			this.tag = tag;
			this.iconRes = iconRes;
		}
	}

	public class CustomAdapter extends ArrayAdapter<CustomItem> {

		public CustomAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.list_item, null);
			}
			ImageView icon = (ImageView) convertView
					.findViewById(R.id.row_icon);
			icon.setImageResource(getItem(position).iconRes);
			TextView title = (TextView) convertView
					.findViewById(R.id.text1);
			title.setText(getItem(position).tag);

			return convertView;
		}

	}
	
	@Override
	public void onListItemClick(HtcListView lv, View v, int position, long id) {
		Fragment newContent = null;
		switch (position) {
		case 0:
			((MainActivity) getActivity()).toggle();
			break;
		case 1:
			Intent intent1 = new Intent(getActivity(), PreferenceActivity.class);
			getActivity().startActivity(intent1);
			break;
		case 2:
			Intent intent2 = new Intent(getActivity(), AppListActivity.class);
			getActivity().startActivity(intent2);
			break;
		}
		if (newContent != null)
			switchFragment(newContent);
	}

	// the meat of switching the above fragment
	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;

		if (getActivity() instanceof MainActivity) {
			MainActivity fca = (MainActivity) getActivity();
			fca.switchContent(fragment);
			
		}
	}

}
