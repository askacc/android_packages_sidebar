package com.hero.sidebar;

import android.content.Intent;
import android.os.Bundle;

import com.hero.sidebar.adapter.AppListActivity;
import com.hero.sidebar.sidebar.SidebarService;
import com.hero.sidebar.util.Util;
import com.htc.preference.HtcPreference;
import com.htc.preference.HtcPreference.OnPreferenceChangeListener;
import com.htc.preference.HtcPreference.OnPreferenceClickListener;
import com.htc.preference.HtcPreferenceActivity;

public class PreferenceActivity extends HtcPreferenceActivity implements
		OnPreferenceClickListener, OnPreferenceChangeListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.IndicatorThemeAlt);
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_in_right, R.anim.fade);
		getPreferenceManager().setSharedPreferencesName(
				Common.KEY_PREFERENCE_MAIN);
		addPreferencesFromResource(R.xml.main_pref);
		findPreference(Common.PREF_KEY_TOGGLE_SERVICE)
				.setOnPreferenceClickListener(this);
		findPreference(Common.PREF_KEY_SIDEBAR_POSITION)
				.setOnPreferenceChangeListener(this);
		findPreference(Common.PREF_KEY_TAB_SIZE).setOnPreferenceChangeListener(
				this);
		findPreference(Common.PREF_KEY_ANIM_TIME)
				.setOnPreferenceChangeListener(this);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onPreferenceClick(HtcPreference p) {
		String k = p.getKey();
		if (k.equals(Common.PREF_KEY_SELECT_APPS)) {
			this.startActivity(
					new Intent(this, AppListActivity.class));
			return true;
		} else if (k.equals(Common.PREF_KEY_TOGGLE_SERVICE)) {
			if (SidebarService.isRunning) {
				SidebarService.stopSidebar(this);
			} else {
				this.startService(
						new Intent(this, SidebarService.class));
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean onPreferenceChange(HtcPreference pref, Object newValue) {
		String k = pref.getKey();
		if (k.equals(Common.PREF_KEY_TAB_SIZE)) {
			Util.refreshService(this);
		} else if (k.equals(Common.PREF_KEY_SIDEBAR_POSITION)
				|| k.equals(Common.PREF_KEY_ANIM_TIME)) {
			Util.resetService(this);
		}
		return true;
	}
	
	@Override
	public void onPause() {
		// Set out transition
		overridePendingTransition(0, R.anim.slide_out_right);
		super.onPause();
	}
}