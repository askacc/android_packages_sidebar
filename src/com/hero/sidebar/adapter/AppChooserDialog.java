package com.hero.sidebar.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;

import com.hero.sidebar.R;
import com.htc.widget.HtcEditText;
import com.htc.widget.HtcListView;
import com.htc.widget.HtcProgressBar;

public abstract class AppChooserDialog extends Dialog {

	final AppChooserAdapter dAdapter;
	final HtcProgressBar dProgressBar;
	final HtcListView dListView;
	final HtcEditText dSearch;
	final ImageButton dButton;
	
	private int mId;
	
	public AppChooserDialog(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_app_chooser_list);
		
		dListView = (HtcListView) findViewById(R.id.listView1);
		dSearch = (HtcEditText) findViewById(R.id.searchText);
		dButton = (ImageButton) findViewById(R.id.searchButton);
		dProgressBar = (HtcProgressBar) findViewById(R.id.progressBar1);
		
		dAdapter = new AppChooserAdapter(context) {
			@Override
			public void onStartUpdate() {
				dProgressBar.setVisibility(View.VISIBLE);
			}
			@Override
			public void onFinishUpdate() {
				dProgressBar.setVisibility(View.GONE);
			}
		};
		dListView.setAdapter(dAdapter);
		dListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
				AppChooserAdapter.AppItem info = (AppChooserAdapter.AppItem) av
						.getItemAtPosition(pos);
				onListViewItemClick(info, mId);
				dismiss();
			}
		});
		dButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dAdapter.getFilter().filter(dSearch.getText().toString());
			}
		});
		dAdapter.update();
	}
	
	public void show(int id) {
		mId = id;
		show();
	}
	
	public abstract void onListViewItemClick(AppChooserAdapter.AppItem info, int id);
}
